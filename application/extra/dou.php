<?php

return [
    'app_id' => "6876312851067651597",
    'app_secret' => "96f79944-29f6-476e-ad33-3de54acf2ab0",

    // 微信获取access_token的url地址
    'access_token_url' => 'https://openapi-fxg.jinritemai.com/oauth2/access_token?app_id=%s&app_secret=%s&grant_type=authorization_self',

    // 获取订单列表api_url
    'order_list' => 'https://openapi-fxg.jinritemai.com/order/list?app_key=%s&access_token=%s&method=order.list&param_json=%s&timestamp=%s&v=2&sign=%s',

    // 订单详情 (单个)
    'order_detail' => 'https://openapi-fxg.jinritemai.com/order/detail?app_key=%s&access_token=%s&method=order.detail&param_json=%s&timestamp=%s&v=2&sign=%s'
];