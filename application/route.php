<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

use think\Route;

// 获取直播间列表
Route::post('api/:version/studio/list', 'api/:version.Studio/allStudio');

// 获取直播间详情
Route::post('api/:version/studio/detail', 'api/:version.Studio/getOne');

//获取token令牌
Route::post('api/:version/token/user', 'api/:version.Token/getToken');

Route::post('api/:version/token/test', 'api/:version.Token/testToken');

// 获取邀请用户的token
Route::post(':api/:version/token/invite', 'api/:version.Token/inviteToken');

//校验令牌
Route::post('api/:version/token/verify', 'api/:version.Token/verifyToken');

// 获取用户手机号码
Route::post('api/:version/token/phone', 'api/:version.Token/getUserPhone');

// 获取分销订单
Route::post('api/:version/order/get', 'api/:version.Order/orders');

// 获取主页信息
Route::post('api/:version/consumer/info', 'api/:version.Consumer/getUserInfo');

// 更新用户信息
Route::post('api/:version/consumer/update', 'api/:version.Consumer/updateInfo');

// 获取分销信息
Route::post('api/:version/distribution/info', 'api/:version.Consumer/getUserDistribution');

// 获取结款账号
Route::post('api/:version/account/info', 'api/:version.Consumer/getAccount');

// 修改结款账号
Route::post('api/:version/account/update', 'api/:version.Consumer/editAccount');

// 下单(当有新订单时，处理分润)
Route::post('api/:version/order/add', 'api/:version.Order/addOrder');

// 推送测试
Route::post('api/:version/push', 'api/:version.Push/index');

// 获取抖店的订单
Route::get('api/:version/order/dou', 'api/:version.Order/getDouOrder');


