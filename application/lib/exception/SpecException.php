<?php

namespace app\lib\exception;

class SpecException extends BaseException
{
	public $code = 404;
	public $msg = '指定规格不存在,请检查参数';
	public $errorCode = 20000;
}