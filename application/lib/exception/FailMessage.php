<?php

namespace app\lib\exception;

class FailMessage
{
	public $msg = 'fail';
	public $error_code = 10001;
}