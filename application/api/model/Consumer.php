<?php


namespace app\api\model;


use think\Db;

class Consumer extends BaseModel
{
    public static function getByPhone($phone)
    {
        $info = self::where('phone', '=', $phone)->field('id, phone')->find();

        return $info;
    }

    public static function getByOpenId($openid)
    {
        $user = self::where('openid','=',$openid)->find();

        return $user;
    }

    // 获取 我的 的数据
    public static function getAllInfo($head_id)
    {
        // 获取分销用户的id
        $consumer_id_arr = Db::name('consumer')->where('consumer_id',$head_id)->column('id');
        $consumer_ids = implode(',', $consumer_id_arr);
        
        $consumer_info = Db::name('consumer')->where('id',$head_id)->field('type, phone')->find();

        $data['id'] = $head_id;
        $data['type'] = intval($consumer_info['type']);
        $data['phone'] = $consumer_info['phone'];

        $sales = Db::name('order')->where('consumer_id','in',$consumer_ids)->where('status','<>','7')->sum('money');
        $data['sales'] = isset($sales) ? $sales : 0;
        $commission = Db::name('order')->where('consumer_id','in',$consumer_ids)->where('status','<>','7')->sum('commission_available');
        $data['commission'] = isset($commission) ? $commission : 0;
        $data['orders'] = Db::name('order')->where('consumer_id','in',$consumer_ids)->count('id');
        $data['refund'] = Db::name('order')->where('consumer_id','in',$consumer_ids)->where('status','7')->count('id');
        $data['distributions'] = Db::name('consumer')->where('consumer_id = '.$head_id.' and type = 1')->count('id');
        $data['users'] = Db::name('consumer')->where('consumer_id = '.$head_id.' and type = 2')->count('id');
        $data['duetime'] = '2020-12-31';

        return $data;
    }

    // 获取 分销 的数据
    public static function getAllDistribution($head_id, $name, $sort)
    {
        $data = Db::name('consumer')
            ->where('consumer_id',$head_id)
            ->order($name,$sort)
            ->field('id, wechatname')
            ->select();

        if ($data) {
            foreach ($data as $k=>&$v){
                $v['commission'] = Db::name('order')
                    ->where('consumer_id and '.$v['id'].' and status <> 7')
                    ->sum('commission_available');
            }
        }

        return $data;

    }

    // 根据指定的父级id查询分销用户数据
    public static function getDistributionById($uid)
    {
        $one_id = self::where('consumer_id', $uid)->field('id,name')->select();
        $two_id = '';

        if(count($one_id)){
            foreach ($one_id as $k=>&$v){
                $v['commission'] = self::getCommissionByConsumerid($v['id']);
                $two_id = self::where('consumer_id', $v['id'])->field('id,name')->select();
                $v['two'] = $two_id;
                $v['nextorders'] = 0;
                if (count($two_id)){
                    foreach ($two_id as $kk=>&$vv){
                        $vv['commission'] = self::getCommissionByConsumerid($vv['id']);
                        $moneys = self::getOrdersByConsumerid($vv['id']);
                        $v['nextorders'] = bcadd($v['nextorders'], $moneys, 2);
                    }
                }
                $v['two'] = $two_id;
            }
        }

        $data = $one_id;

        return $data;
    }

    public static function getCommissionByConsumerid($id)
    {
        $data = Db::name('order')
            ->where('consumer_id', $id)
            ->order('ordertime desc')
            ->sum('commission');

        if (!$data){
            $data = 0;
        }

        return $data;
    }

    public static function getOrdersByConsumerid($id)
    {
        $data = Db::name('order')
            ->where('consumer_id', $id)
            ->order('ordertime desc')
            ->sum('money');

        if (!$data){
            $data = 0;
        }

        return $data;
    }

    // 获取结款账号信息
    public static function getAccountInfo($uid)
    {
        $data = self::where('id', $uid)->field('cardholder, cardnumber, bank')->find();

        return $data;
    }

    // 根据head_id 和type 更新数据
    public static function updateInfo($uid,$head_id,$type)
    {
        $dealer_id = self::where('id', $head_id)->field('dealer_id')->find();

        if ($dealer_id){
            $update['dealer_id'] = $dealer_id['dealer_id'];
        }
        $update['type'] = $type;
        $update['consumer_id'] = $head_id;

        $data = self::where('id', $uid)->update($update);

        return $data;
    }

}