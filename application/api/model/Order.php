<?php 

namespace app\api\model;

use think\Db;

class Order extends BaseModel
{
	protected $hidden = ['createtime', 'updatetime', 'deletetime'];
	protected $autoWriteTimestamp = true;

	// protected $createTime = 'create_timestamp';

    public function getStatusAttr($value,$data)
    {
        if ($value == 0){
            return '未支付';
        }elseif ($value == 1){
            return '未发货';
        }elseif ($value == 2){
            return '已发货';
        }elseif ($value == 3){
            return '已签收';
        }elseif ($value == 4){
            return '退款';
        }
    }

    public function placeOrder()
    {
        return $this->belongsTo('consumer','consumer_id','id');
    }

    // 获取分销订单
    public static function getOrders($head_id)
    {
        // 获取分销用户的id
        $consumer_id_arr = Db::name('consumer')->where('consumer_id',$head_id)->column('id');
        $consumer_ids = implode(',', $consumer_id_arr);

        $where_sql['o.consumer_id'] = ['in',$consumer_ids];
        $where_sql['o.status'] = ['<>','7'];

        $data = Db::name('order')
            ->alias('o')
            ->join('shop s','o.shop_id = s.shop_id','LEFT')
            ->join('consumer c','o.consumer_id = c.id','LEFT')
            ->where($where_sql)
            ->field(
                'o.id,o.goods,o.status,o.money,o.commission_available as commission,
                o.ordertime,o.goodsimage,o.shop_id,
                s.name as shopname,o.receiver as placeordername'
            )
            ->select();

        foreach ($data as &$v){
            switch ($v['status'])
            {
                case 1:
                    $v['status'] = '未支付';
                    break;
                case 2:
                    $v['status'] = '未发货';
                    break;
                case 3:
                    $v['status'] = '已发货';
                    break;
                case 4:
                    $v['status'] = '已取消';
                    break;
                case 5:
                    $v['status'] = '已完成';
                    break;
                case 6:
                    $v['status'] = '退款中';
                    break;
                case 7:
                    $v['status'] = '已退款';
                    break;
            }
            if (!$v['shopname']){
                $v['shopname'] = '抖音店铺_'.$v['shop_id'];
            }
        }

        return $data;
    }

    // 随便5条订单数据
    public static function getCasualOrders()
    {
        $data = Db::name('order')
            ->alias('o')
            ->join('shop s','o.shop_id = s.id')
            ->join('consumer c','o.consumer_id = c.id')
            ->limit(5)
            ->field(
                'o.id,o.goods,o.status,o.money,o.commission,o.ordertime,o.goodsimage,
                s.name as shopname,c.name as placeordername'
            )
            ->select();
        foreach ($data as &$v){
            $v['goodsimage'] = config('setting.img_prefix').$v['goodsimage'];
            switch ($v['status'])
            {
                case 0:
                    $v['status'] = '未支付';
                    break;
                case 1:
                    $v['status'] = '未发货';
                    break;
                case 2:
                    $v['status'] = '已发货';
                    break;
                case 3:
                    $v['status'] = '已签收';
                    break;
                case 4:
                    $v['status'] = '退款';
                    break;
            }
        }

        return $data;
    }

	// 根据用户id获取历史订单
	public static function getSummaryByUser($uid, $page=1, $size=15)
	{
		$pagingData = self::where('user_id', '=', $uid)
			->order('create_time desc')
			->paginate($size, true, ['page' => $page]);

		return $pagingData;
	}

	// 获取器 把订单详情中获取的部分数据转换成json对象格式
	public function getSnapItemsAttr($value)
	{
		if(empty($value)){

			return null;
		}

		return json_decode($value);
	}

	public function getSnapAddressAttr($value)
	{
		if(empty($value)){
			return null;
		}

		return json_decode($value);
	}

	public static function getSummaryByPage($page=1, $size=10)
	{
		$pagingData = self::order('create_time desc')
			->paginate($size, true, ['page' => $page]);

		return $pagingData;
	}
}