<?php 

namespace app\api\model;

class User extends BaseModel
{
	protected $table = 'wx_user';

	protected $hidden = ['update_time','extend','delete_time'];

	public function address()
	{
		return $this->hasMany('userAddress', 'user_id', 'id');
	}

	public static function getByOpenId($openid)
	{
		$user = self::where('openid','=',$openid)->find();

		return $user;
	}
}