<?php


namespace app\api\controller\v1;


use app\api\controller\BaseController;
use app\api\service\Token as TokenService;
use app\api\model\Consumer as ConsumerModel;
use app\api\validate\Account;
use app\api\validate\Consumer as ConsumerValidate;
use app\lib\exception\FailMessage;
use app\lib\exception\SuccessMessage;

class Consumer extends BaseController
{
    protected $beforeActionList = [
        'checkPrimaryScope' => ['only' => '']
    ];

    /**
     * 获取 我的 数据量
     */
    public function getUserInfo()
    {
        $uid = TokenService::getCurrentUid();
        // $uid = 5;
        $data = ConsumerModel::getAllInfo($uid);
        $result = [];

        $result['msg'] = 'success';
        $result['error_code'] = 0;
        $result['data'] = $data;

        return $result;
    }

    /**
     * 获取 分销 的数据
     */
    public function getUserDistribution($name = 'id', $sort='asc')
    {
        $uid = TokenService::getCurrentUid();
//        $uid = 1;
        $data = ConsumerModel::getAllDistribution($uid, $name,$sort);
        $result = [];

        $result['msg'] = 'success';
        $result['error_code'] = 0;
        $result['data'] = $data;

        return $result;
    }

    /**
     * 获取 [我的] 信息
     */


    /**
     * 结款账号
     */
    public function getAccount()
    {
        $uid = TokenService::getCurrentUid();
//        $uid = 1;
        $data = ConsumerModel::getAccountInfo($uid);
        $result = [];

        $result['msg'] = 'success';
        $result['error_code'] = 0;
        $result['data'] = $data;

        return $result;
    }

    /**
     * 更改结款账号
     */
    public function editAccount()
    {
        $uid = TokenService::getCurrentUid();
//        $uid = 5;

        $validate = new Account();
        $validate->goCheck();

        $dataArray = $validate->getDataByRule(input('post.'));

        $update = ConsumerModel::where('id', $uid)->update($dataArray);

        if($update !== false){
            return new SuccessMessage();
        }else{
            return new FailMessage();
        }
    }

    /**
     * 更新用户信息
     */
    public function updateInfo()
    {
        $uid = TokenService::getCurrentUid();
//        $uid = 2;

        $validate = new ConsumerValidate();
        $validate->goCheck();

        $dataArray = $validate->getDataByRule(input('post.'));

        $update = ConsumerModel::where('id', $uid)->update($dataArray);

        if($update !== false){
            return new SuccessMessage();
        }else{
            return new FailMessage();
        }
    }
}