<?php


namespace app\api\controller\v1;

use app\api\service\Token as TokenService;
use app\api\model\Studio as StudioModel;
use app\api\validate\IDMustBePostiveInt;
use think\Db;

class Studio
{
    public function allStudio()
    {
        $uid = TokenService::getCurrentUid();
//        $uid = 1;

        // 获取所属经销商的id
        $dealer = Db::name('consumer')->where('id', $uid)->field('dealer_id')->find();

        $data = StudioModel::allStu($dealer['dealer_id']);
        $result = [];

        $result['msg'] = 'success';
        $result['error_code'] = 0;
        $result['data'] = $data;

        return $result;
    }

    public function getOne($id='')
    {
        (new IDMustBePostiveInt())->goCheck();

        $data = StudioModel::getOne($id);

        $result = [];
        $result['msg'] = 'success';
        $result['error_code'] = 0;
        $result['data'] = $data;

        return $result;
    }
}