<?php


namespace app\api\validate;


class ParamMustBeInt extends BaseValidate
{
    protected $rule = [
        'num' => 'require|isInteger',
        'content' => 'require|isNotEmpty',
        'is_exchange' => 'require|in:0,1'
    ];

    protected $message = [
        'num' => 'num必须是整数',
        'content' => '积分来源不能为空',
        'is_exchange' => 'is_exchange必须是0或1'
    ];
}