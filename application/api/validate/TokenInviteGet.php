<?php


namespace app\api\validate;


class TokenInviteGet extends BaseValidate
{
    protected $rule = [
        'head_id'   => 'require|isNotEmpty',
        'type'  => 'require|isNotEmpty'
    ];
}