<?php


namespace app\api\validate;


class ExchangeMustBeInt extends BaseValidate
{
    protected $rule = [
        'is_exchange' => 'require|in:0,1'
    ];

    protected $message = [
        'is_exchange' => 'is_exchange必须是0或1'
    ];
}