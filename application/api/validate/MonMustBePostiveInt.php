<?php


namespace app\api\validate;


class MonMustBePostiveInt extends BaseValidate
{
    protected $rule = [
        'mon' => 'require|isPositiveInteger',
    ];

    protected $message = [
        'mon' => 'mon必须是正整数'
    ];
}