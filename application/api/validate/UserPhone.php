<?php 

namespace app\api\validate;

/**
 * 
 */
class UserPhone extends BaseValidate
{
	
	protected $rule = [
		'encryptedData' => 'require|isNotEmpty',
		'iv' => 'require|isNotEmpty',
        'wechatname' => 'require|isNotEmpty',
        'headavatar' => 'require|isNotEmpty',
	];
}