<?php 

namespace app\api\validate;

class Character extends BaseValidate
{
	protected $rule = [
		'name' => 'require|isNotEmpty',
		'phone' => 'require|isMobile',
	];

	protected $message = [
		'phone' => '手机格式不正确'
	];
}