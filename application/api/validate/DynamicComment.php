<?php


namespace app\api\validate;


class DynamicComment extends BaseValidate
{
    protected $rule = [
        'id' => 'require|isPositiveInteger',
        'content' => 'require|isNotEmpty'
    ];
}