<?php


namespace app\api\validate;


class SchoolUser extends BaseValidate
{
    protected $rule = [
      'phone' => 'require|isMobile',
      'password' => 'require|isNotEmpty'
    ];

    protected $message = [
        'phone' => '号码错误',
        'password' => '密码错误'
    ];
}