<?php


namespace app\api\validate;


class Consumer extends BaseValidate
{
    protected $rule = [
        'wechatname' => 'require|isNotEmpty',
        'headavatar' => 'require|isNotEmpty',
        'phone' => 'require|isNotEmpty'
    ];
}